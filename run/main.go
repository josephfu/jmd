package main

import (
	"strings"
	"time"
	"fmt"
	"strconv"
	"os"
	"reflect"
	"math/rand"
	"encoding/json"
	"io/ioutil"
	"github.com/go-zeromq/zmq4"
)

func main() {
	saveLog( "main" , true , true , "main" , "starting process..." )
	readConfigFile()
	addSymList()

	go subZmq( gDomain , gPortSt.subDQuote , "D" , gSubDQuoteCh )
	go subZmq( gDomain , gPortSt.subJmd2 , "CMD" , gSubJmdCh )
	go pubZmq( gDomain , gPortSt.pubJmd , gPubJmdCh )
	go subZmq( gDomain , gPortSt.subTrade , "R" , gSubDTradeCh )
	go storeData()
	go checkTickVol()
	go initConfigTimer()

	go testFunc()
	go pushFakeCMD( "CMR 301 MSG a_tabMsg Testing Msg From ZMQ!" )
	go pushFakeCMD( "CMR 301 JMD TEST Testing Msg From ZMQ!" )
	// go pushFakeQuote()

	for {
		select {
			case dQuote := <- gSubDQuoteCh :
				go readDQuote( dQuote )
			case rQuote := <- gSubDTradeCh :
				go readRQuote( rQuote )
			case msg := <- gPubJmdCh :
				go sendZmqMsg( msg , gPubJmdPub )
			case subJmd := <- gSubJmdCh :
				go readJmdMsg( subJmd )
		}
	}
}

func readJmdMsg( m string ) {
	var ary = strings.Split( m , " " )
	if len( ary ) <= 4 || ary[ 2 ] != "JMD" {
		return
	}
	saveLog( "jmdMsg" , true , true , "jmdMsg" , m )

	var uid , _ = strconv.Atoi( ary[ 1 ] )
	var jmd = ary[ 3 ]
	if jmd == "GET_CONFIG" {
		announceConfigJMD( uid )
	}else if jmd == "CONFIG"{
		readJMDConfig( ary )
	}
}

func readJMDConfig( ary []string ){
	var uid , _ = strconv.Atoi( ary[ 1 ] )
	var name = strings.Title( strings.Split( ary[ 4 ] , "a_" )[ 1 ] )
	var parm = ary[ 5 ]

	for i := 0 ; i < len ( gConfSt.Users ) ; i++ {
		if gConfSt.Users[ i ].Uid != uid {
			continue
		}
		setStruct( gConfSt.Users[ i ] , &gConfSt.Users[ i ] , name , parm )
		break
	}

	resetConfigTimer()
}

func announceConfigJMD( uid int ){
	saveLog( "error" , true , true , "main" , "announceConfigJMD" )
	for i := 0 ; i < len ( gConfSt.Users ) ; i++ {
		if gConfSt.Users[ i ].Uid != uid {
			continue
		}

		t := reflect.TypeOf( gConfSt.Users[ i ] )
		v := reflect.ValueOf( gConfSt.Users[ i ] )

		for j := 0 ; j < t.NumField() ; j++ {
			var name = t.Field( j ).Name
			var vType = t.Field( j ).Type.String()
			var value = v.Field( j )
			var vString = ""

			if vType == "int"{
				vString = strconv.FormatInt( value.Int() , 10 )
			}else if vType == "float64"{
				vString = strconv.FormatFloat( value.Float() , 'f' , 1 , 64 )
			}else if vType == "string"{
				vString = value.String()
			}else if vType == "bool"{
				vString = strconv.FormatBool( value.Bool() )
			}else{
				vString = ""
			}

			var jmd = "CMD " + strconv.Itoa( uid ) + " JMR CONFIG " + name + " " + vString
			pushJMD ( jmd )
		}
		break
	}
}

func readDQuote( m string ) {
	var ary = strings.Split( m , " " )
	if len( ary ) <= 2 {
		return
	}
	saveLog( "dQuote" , true , gShowDQuoteFmt , "dQuote" , m )

	var symAry = strings.Split( ary[ 2 ] , "_" )
	var symType = symAry[ 0 ]

	quoteK := func() {
		storeQuoteData( ary )
	}
	quoteMA := func() {
	}
	quoteInd := func() {
	}
	quoteNM := func() {
	}

	switch symType {
		case "K" :
			quoteK()
		case "MA" :
			quoteMA()
		case "IND" :
			quoteInd()
		case "RL" :
			break
		default :
			quoteNM()
	}
}

func readRQuote( m string ) {
	saveLog( "rQuote" , true , gShowRQuoteFmt , "rQuote" , m )
}

func sendZmqMsg( m string , pub zmq4.Socket ) {
	if m == "" {
		return
	}
	zMsg := zmq4.NewMsgFrom(
		[]byte( m ) ,
	)
	err := pub.Send( zMsg )
	if err != nil {
		saveLog( "error" , true , true , "sendZmqMsg" , "Could not send pub msg. " + err.Error() )
	}
	saveLog( "main" , true , true , "sendZmqMsg" , m )
}

func pushFakeCMD( m string ) {
	time.Sleep( 2 * time.Second )
	gPubJmdCh <- m
}

func pushJMD( jmd string ){
	gPubJmdCh <- jmd
}

func storeQuoteData( ary []string ) {
	var ecn = ary[ 1 ]
	var oSym = ary[ 2 ]
	var sym = chopSymStr( oSym )

	var onList = false
	for i :=0 ; i<len( gSymListAry ) ; i++ {
		if ecn != gSymListAry[ i ][ 0 ] {
			continue
		}
		if sym != gSymListAry[ i ][ 1 ] {
			continue
		}
		onList = true
		break
	}
	if !onList {
		return
	}

	var px , _ = strconv.ParseFloat( ary[ 3 ] , 64 )
	var vol , _ = strconv.Atoi( ary[ 5 ] )

	var newSym = true

	for i := 0 ; i < len( gQuoteMap ) ; i++ {
		if ecn != gQuoteMap[ i ].ecn {
			continue
		}
		if sym != gQuoteMap[ i ].sym {
			continue
		}
		newSym = false
		var sct = gQuoteMap[ i ] ;
		sct.px = px
		sct.vol = vol
		gQuoteMap[ i ] = sct
	}
	if newSym {
		data := gQuoteSt { ecn , sym , px , []float64 {} , vol , []int {} }
		gQuoteMap[ len( gQuoteMap ) ] = data
	}

	checkTickPx()
	checkTickVol()
}

func storeData() {
	var maxLen = 100
	for {
		for i := 0 ; i < len( gQuoteMap ) ; i++ {
			var sct = gQuoteMap[ i ] ;
			sct.pxAry = append( sct.pxAry , sct.px )
			sct.volAry = append( sct.volAry , sct.vol )
			if len( sct.pxAry ) > maxLen {
				sct.pxAry = sct.pxAry[ 1 : ]
			}
			if len( sct.volAry ) > maxLen {
				sct.volAry = sct.volAry[ 1 : ]
			}
			gQuoteMap[ i ] = sct
		}
		time.Sleep( 1 * time.Second / 5 )
	}
}

func pushFakeQuote() {
	var mtype = "D"
	var mEcn = "MREX"
	var mSym = "FAKE_SYM"
	var mPx = 1000.0
	var mVol = 0
	var msg string
	var rPx string
	var rVol string
	for {
		mPx = mPx + ( rand.Float64() * 10 ) - 5
		rPx = strconv.FormatFloat( mPx , 'f' , 1 , 64 )

		mVol = mVol + rand.Intn( 100 )
		rVol = strconv.Itoa( mVol )

		msg = mtype + " " + mEcn + " " + mSym + " " + rPx + " testing " + rVol
		gSubDQuoteCh <- msg
		time.Sleep( 1 * time.Second / 5 )
	}
}

func checkTickPx() {
	var tick = 10
	var limit = 20.0
	var pxAry = gQuoteMap[ 0 ].pxAry
	if len( pxAry ) < 12 {
		return
	}
	var last = pxAry[ len( pxAry ) - 1 ]
	var prev = pxAry[ len( pxAry ) - 1 - tick ]
	var chg = last - prev

	if chg < limit {
		return
	}
	fmt.Println( "Price change > " + strconv.FormatFloat( limit , 'f' , 1 , 64 ) + " , chged : " + strconv.FormatFloat( chg , 'f' , 1 , 64 ) )
}

func checkTickVol() {
	var tick = 10
	var limit = 700
	var volAry = gQuoteMap[ 0 ].volAry
	if len( volAry ) < 12 {
		return
	}
	var last = volAry[ len( volAry ) - 1 ]
	var prev = volAry[ len( volAry ) - 1 - tick ]
	var chg = last - prev

	if chg < limit {
		return
	}
	fmt.Println( "Volume change > " + strconv.Itoa( limit ) + " , chged : " + strconv.Itoa( chg ) )
}

func chopSymStr( oSym string ) string {
	i := strings.Index( oSym , "_" )
	if i == -1 {
		return oSym
	}
	return oSym[ i + 1 : ]
}

func testFunc() {
}

func resetConfigTimer() {
	gConfTimer = time.NewTimer( 10 * time.Second )
	gConfTimerCh <- 0
}

func initConfigTimer() {
	var updated = false
	gConfTimer = time.NewTimer( 0 )
	for {
		select {
			case <-gConfTimer.C:
				if !updated{
					updated = true
					continue
				}
				updateConfigFile()
			case <-gConfTimerCh:
				gConfTimer.Reset( 2 * time.Second )
		}
	}
}

func setStruct ( st interface{} , stp interface{} , name string , value string ) {
	t := reflect.TypeOf( st )
	tField,_ := t.FieldByName( name )
	tType := tField.Type.String()

	v := reflect.ValueOf( stp ).Elem()
	vField := v.FieldByName( name )

	if tType == "int"{
		i,_ :=  strconv.ParseInt( value , 10, 64)
		vField.SetInt( i )
	}else if tType == "float64"{
		f,_ := strconv.ParseFloat( value , 64)
		vField.SetFloat( f )
	}else if tType == "string"{
		vField.SetString( value )
	}else if tType == "bool"{
		b,_ := strconv.ParseBool( value )
		vField.SetBool( b )
	}
}

func readConfigFile() {
	saveLog( "main" , true , true , "main" , "reading config file..." )
	file , err := os.Open( "conf.json" )
	if err != nil {
		saveLog( "error" , true , true , "readConfigFile" , "Could not open config file" )
	}
	defer file.Close()
	data , err := ioutil.ReadAll( file )
	if err != nil {
		saveLog( "error" , true , true , "readConfigFile" , "Could not read config file" )
	}
	gConfSt = gConfUsersSt {}
	json.Unmarshal( data , &gConfSt )
}

func updateConfigFile() {
	saveLog( "main" , true , true , "main" , "update config file..." )
	file , err := os.Create( "conf.json" )
	if err != nil {
		saveLog( "error" , true , true , "readConfigFile" , "Could not create config file" )
	}
	data , err := json.Marshal( gConfSt )
	if err != nil {
		saveLog( "error" , true , true , "readConfigFile" , "Could not marshal config file" )
	}
	file.Write( data )
	file.Close()
}