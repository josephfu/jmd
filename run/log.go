package main

import (
	"os"
	"log"
	"time"
	"fmt"
)

func saveLog( path string , withDate bool , withFmt bool , title string , msg string ){
	if withDate{
		date := time.Now().Local().Format("20060102")
		path = path + "_" + date
	}
	if withFmt{
		printFmt( title , msg )
	}
	path = "../log/" + path + ".log"

	file, err := os.OpenFile( path ,os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		file, err = os.Create( path )
		if err != nil {
			panic(err)
		}
	}
	defer file.Close()

	log.SetOutput( file )
	log.SetPrefix( title + ": " )
	log.Println( msg )
}

func printFmt( title string , msg string ){
	if !gShowFmt{
		return
	}
	fmt.Println( title + ": " + msg )
}