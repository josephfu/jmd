package main

import (
	"github.com/go-zeromq/zmq4"
	"context"
	"time"
)

var gShowFmt = true
var gShowRQuoteFmt = false
var gShowDQuoteFmt = false
var gPortSt = struct{
	pubJmd int
	subJmd int
	subJmd2 int
	subDQuote int
	subTrade int
	pubOrder int
}{
	19002 ,
	19004 ,
	19007 ,
	19101 ,
	19916 ,
	19924 ,
}

// var gDomain = "192.168.1.187"
var gDomain = "127.0.0.1"
var gSubJmdCh = make( chan string )
var gSubDQuoteCh = make( chan string )
var gPubJmdCh = make( chan string )
var gSubDTradeCh = make( chan string )
var gPubJmdPub = zmq4.NewPub( context.Background() )

var gQuoteMap = make( map[ int ] gQuoteSt )
type gQuoteSt struct{
	ecn string
	sym string
	px float64
	pxAry []float64
	vol int
	volAry []int
}
var gSymListAry = [][]string{}
func addSymList() {
	gSymListAry = append( gSymListAry , []string{ "MREX" , "CME/CMX_GLD/DEC18" } )
	gSymListAry = append( gSymListAry , []string{ "MREX" , "CME/CMX_SIL/DEC18" } )
	gSymListAry = append( gSymListAry , []string{ "HKE" , "HSIV8" } )
	gSymListAry = append( gSymListAry , []string{ "MREX" , "FAKE_SYM" } )
}

type gConfUsersSt struct {
	Users []gConfUserSt
}
type gConfUserSt struct {
	Uid int
	PanlCfg_refEcn string
	PanlCfg_refSym string
	PanlCfg_tradeEcn string
	PanlCfg_tradeSym string
	GuardCfg_MaxFiveSecVol_parm int
	GuardCfg_MaxFiveSecVol_on bool
	GuardCfg_MaxFiveSecVol_pause bool
	GuardCfg_MaxFiveSecVol_off bool
	GuardCfg_MaxFiveSecVol_flat bool
	GuardCfg_MaxFiveSecVol_flatAll bool
	GuardCfg_MaxFiveSecVol_notice bool
}

var gConfSt gConfUsersSt
var gConfTimer *time.Timer
var gConfTimerCh = make( chan int )