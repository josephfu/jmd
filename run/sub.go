package main

import (
	"github.com/go-zeromq/zmq4"
	"context"
	"strconv"
)

func subZmq( domain string , port int , filter string , ch chan string ) {
	var host = "tcp://" + domain + ":" + strconv.Itoa( port )
	sub := zmq4.NewSub( context.Background() )
	defer sub.Close()
	err := sub.Dial( host )
	if err != nil {
		saveLog( "error" , true , true , "subZmq" , "Could not dial to host: " + host + ". " + err.Error() )
	}
	saveLog( "main" , true , true , "subZmq" , "sub to: " + host )

	err = sub.SetOption( zmq4.OptionSubscribe , filter )
	if err != nil {
		saveLog( "error" , true , true , "subZmq" , "Error on set sub option. " + err.Error() )
	}

	for {
		rawMsg , err := sub.Recv()
		if err != nil {
			saveLog( "error" , true , true , "subZmq" , "Could not receive message. " + err.Error() )
		}
		var msg = string( rawMsg.Frames[0] )
		ch <- msg
	}
}