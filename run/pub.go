package main

import (
	"github.com/go-zeromq/zmq4"
	"context"
	"strconv"
)

func pubZmq( domain string , port int , ch chan string ) {
	var host = "tcp://" + domain + ":" + strconv.Itoa( port )
	gPubJmdPub = zmq4.NewPub( context.Background() )
	err := gPubJmdPub.Dial( host )
	if err != nil {
		saveLog( "error" , true , true , "pubZmq" , "Could not publish to host: " + host + ". " + err.Error() )
	}
	saveLog( "main" , true , true , "pubZmq" , "pub to: " + host )
}


